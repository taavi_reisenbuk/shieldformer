﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour {

	public Enemy enemy;
	public GameObject explosionEffect;
	private bool isDead = false;

	private void Update() {
		if (enemy == null) {
			isDead = true;
		}

		if (isDead) {
			Explode(this.transform.position);
		}
	}

	public void Explode(Vector3 position) {
		Instantiate(explosionEffect, position, Quaternion.identity);
		Destroy(this.gameObject);
	}
}
