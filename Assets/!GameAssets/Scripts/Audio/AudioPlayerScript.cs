﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioPlayerScript : MonoBehaviour {

    public static AudioPlayerScript audioPlayerInstance;
    public AudioClipGroup ambientAudio;
    public AudioClipGroup CharacterWalkingSounds;
    public AudioClipGroup jumpingSounds;
    public AudioClipGroup shieldHitSounds;
    public AudioClipGroup playerShootingSounds;
    public AudioClipGroup enemyShootingSounds;
	public AudioClipGroup enemyAlertedSounds;
    public AudioClipGroup playerHitSounds;
	public AudioClipGroup playerDashSounds;
	public AudioClipGroup burstEnemyShootingSound;
	public AudioClipGroup rapidEnemyShootingSound;
	public AudioClipGroup enemyShotAgainstGround;
    public Player player;

	// Use this for initialization
	void Start () {
        audioPlayerInstance = this;
    }
	
	// Update is called once per frame
	void Update () {

        if (player.isMoving() & player.isGrounded())
        {
            CharacterWalkingSounds.Play();
        }

        if (player.jumpSound())
        {
            jumpingSounds.Play();
        }

        ambientAudio.Play(); 
	}


    public void playShieldIsHitSound()
    {
        shieldHitSounds.Play();
    }

    public void playPlayerShootingSound()
    {
        playerShootingSounds.Play();
    }

    public void playEnemyShootingSound()
    {
        enemyShootingSounds.Play();
    }


	public void playEnemyAlertedSound() {
		enemyAlertedSounds.Play();
	}

	public void playPlayerHitSound()
    {
        playerHitSounds.Play();
    }

	public void playPlayerDashSound() {
		playerDashSounds.Play();
	}

	public void playBurstEnemyShootingSound() {
		burstEnemyShootingSound.Play();
	}

	public void playRapidEnemyShootingSound() {
		rapidEnemyShootingSound.Play();
	}

	public void playEnemyShotAgainstGround() {
		enemyShotAgainstGround.Play();
	}
}
