﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Shield : MonoBehaviour {

	// Particle system in this class.

	public ShieldShotPoint shotPoint;
	public ParticleSystem shieldParticleSystem;
	public Transform shieldPivotPoint;
    static float energyAmount = 100;

	// Use this for initialization
	void Start () {
        energyAmount = 0;
	}
	
	// Update is called once per frame
	void Update () {

        if (energyAmount > 0)
        {
            energyAmount -= 1f * Time.deltaTime;
        }
	}


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("enemyProjectile"))
        {
            energyAmount += 40;
            if (energyAmount > 100)
            {
                energyAmount = 100;
            }
            if (energyAmount < 0)
            {
                energyAmount = 0;
            }

			// Hävita vastase projectile. Particleystem emitter.
			ParticleSystem ps = Instantiate(shieldParticleSystem);
			ps.transform.position = collision.transform.position;


			float v = shieldPivotPoint.transform.eulerAngles.z;
			float particleSystemXpos = translatePivotPointToParticleSystemRotation(v);

			// Debug.Log(v + " " + particleSystemXpos);
			ps.transform.rotation = Quaternion.Euler(particleSystemXpos, 90, 0);
			ps.Play();
			Destroy(collision.gameObject);
			AudioPlayerScript.audioPlayerInstance.playShieldIsHitSound();

			// TODO: Particle system isn't destroying itself. Memory leak source.
			Destroy(ps, 2f); // Destroy isn't working for some reason.
		}
    }

	private float translatePivotPointToParticleSystemRotation(float pivotPointZ) {
		return (270 - pivotPointZ);
	}

    public float getEnergyAmount()
    {
        return energyAmount;
    }

	public void setEnergyAmount(float en) {
		energyAmount = en;
	}

	public void maxEnergy()
    {
        energyAmount = 100;
    }

    public void changeEnergylevel(float change)
    {
        energyAmount += change;
    }
}
