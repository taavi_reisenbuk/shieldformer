﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public static Player playerInstance;
    public float jumpForce = 400f;
    public float movementSmoothing = 0.05f;
    public bool enableAirMovement = false;
    public float[] checkpoint;

    public GameObject cameraShake; // Makes camera shake
	public GameObject shotEffect;

    public LayerMask ground;
    public Transform checkGround;
    private float groundedRadius = 0.2f;
    private bool grounded;
	private bool falling;

    private Rigidbody2D rb2;
    private Vector3 velocity = Vector3.zero;

    public float movementSpeed;
    private bool controlEnabled = true;

    private bool moving;
    private bool jumped = false; // Ainult jump soundi mängimiseks.

    static int health = 100;

    public Shield shield;
    public GameObject projectile;
    public float shotSpeed;
    private float time;
    public float attackCooldown;
	public bool powerfulAttackAvailable;

    private SpriteRenderer sr;
    public Animator animator;

	// Double jumping
	public int numOfJumps = 0;
	public int maxJumps = 2;

	// Dashing
	public bool dashAvailable;
	public float dashSpeed;
	private float dashTime;
	public float startDashTime;
	private int dashDirection;
	public ParticleSystem dashEffect;
    private bool dash;

    // Invulnerability after getting hit
    private bool invulnerable = false;
    private float invTime = 0.0f;

	private void Awake() {
		rb2 = GetComponent<Rigidbody2D>();
	}

	public void Start()
    {
        playerInstance = this;
        sr = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
		dashTime = startDashTime;
        //LoadPlayer();
    }

    void Update(){

        if (health <= 0)
            LoadPlayer();

		if (health >= 100) health = 100;
		if (shield.getEnergyAmount() >= 100) shield.setEnergyAmount(100);

        Debug.DrawRay(transform.position, Vector3.forward, Color.red, 20, true);
		time += Time.deltaTime;

		if (Input.GetKeyUp(KeyCode.C)) {
			dash = false;
		}

		if (rb2.velocity.y < 0) {
			falling = true;
		}
		else falling = false;

		if (grounded) {
			numOfJumps = 0;
		}

		jumped = false;
        moving = false;
        float horizontal = Input.GetAxis("Horizontal");

        // Movement and flipping character based on direction.
        if (Math.Abs(horizontal) > float.Epsilon)
        {
            sr.flipX = horizontal > 0f;
            moving = true;
        }


        if (controlEnabled)
        {
			// Liigub vasakule.
            if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D)) {
				Move(movementSpeed);
				if (dashDirection == 0) dashDirection = 1;
            }

			// Liigub paremale.
            else if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A)) {
				Move(-movementSpeed);
				if (dashDirection == 0) dashDirection = 2;
            }

			// Hüppamine.
			if (Input.GetKeyDown(KeyCode.Space)) {
				if (numOfJumps < maxJumps) {
					numOfJumps++;
					Jump();
					jumped = true;
					jumpSound();
				}
            }

			// See tähendab, et parasjagu käib dash
        }

        if (Input.GetKey(KeyCode.Mouse0) && shield.getEnergyAmount() >= 5 && time >= attackCooldown)
        {
            Attack();
            shield.changeEnergylevel(-40f);
            time = 0;
        }

        if (invulnerable)
        {
            invTime += Time.deltaTime;
            if (invTime >= 1.5f)
            {
                toggleInvulnerable();
            }
        }

		animator.SetBool("airborne", !grounded);
		animator.SetBool("falling", falling);
		animator.SetBool("jumped", jumped);
        animator.SetBool("Walk", moving);
    }

    private void FixedUpdate()
    {
        grounded = false;
        Collider2D[] colliders = Physics2D.OverlapCircleAll(checkGround.position, groundedRadius, ground); 
        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i].gameObject != gameObject)
            {
                grounded = true;
            }
        }

		// Dashing
		if (dashAvailable) {
			if (dashDirection != 0) {
				if (dashTime <= 0) {
                    // dash = true;
					dashDirection = 0;
					dashTime = startDashTime;
					rb2.velocity = new Vector2(0f, rb2.velocity.y);
				}
				else if (shield.getEnergyAmount() >= 5) {
					dashTime -= Time.deltaTime;
					if (dashDirection == 1 & Input.GetKey(KeyCode.C)) {
                        dash = true;
						rb2.velocity = new Vector2(dashSpeed, rb2.velocity.y);
						ParticleSystem dashPS = Instantiate(dashEffect, transform.position, Quaternion.identity);
						dashPS.Play();
						AudioPlayerScript.audioPlayerInstance.playPlayerDashSound();
						shield.changeEnergylevel(-70 * Time.deltaTime);
					}
					else if (dashDirection == 2 & Input.GetKey(KeyCode.C)) {
                        dash = true;
                        rb2.velocity = new Vector2(-dashSpeed, rb2.velocity.y);
						ParticleSystem dashPS = Instantiate(dashEffect, transform.position, Quaternion.identity);
						dashPS.Play();
						AudioPlayerScript.audioPlayerInstance.playPlayerDashSound();
						shield.changeEnergylevel(-70 * Time.deltaTime);
					}
				}
			}
		}

		animator.SetBool("dash", dash);
    }

    public void SavePlayer()
    {
        PlayerPrefs.SetFloat("x", this.transform.position.x);
        PlayerPrefs.SetFloat("y", this.transform.position.y);
        PlayerPrefs.SetFloat("z", this.transform.position.z);
        PlayerPrefs.SetString("Level", SceneManager.GetActiveScene().name);
      }

    public void LoadPlayer()
    {
        Debug.Log("loadplayer" + PlayerPrefs.GetString("Level"));

        SceneManager.LoadScene(PlayerPrefs.GetString("Level"));

        Shield shield = FindObjectOfType<Shield>();
        shield.maxEnergy();
        health = 1000;

        Vector3 position = new Vector3();

        position.x = PlayerPrefs.GetFloat("x");
        position.y = PlayerPrefs.GetFloat("y");
        position.z = PlayerPrefs.GetFloat("z");

        Debug.Log("x: " + position.x);
        Debug.Log("y: " + position.y);
        Debug.Log("z: " + position.z);
        
        this.transform.position = position;
    }


    public void Jump()
	{
		grounded = false;
		rb2.velocity = new Vector2(rb2.velocity.x, 0);
		rb2.AddForce(new Vector2(0f, jumpForce));
		numOfJumps += 1;
	}

	public void Move(float move)
	{
		if (grounded || enableAirMovement)	{
            Vector3 targetVelocity = new Vector2(move, rb2.velocity.y);
            rb2.velocity = Vector3.SmoothDamp(rb2.velocity, targetVelocity, ref velocity, movementSmoothing);
        }
    }

    public void SetControlEnabled(bool b)
	{
		controlEnabled = b;
	}

    // Damage to the player
    public void Damage(int damage)
    {
        health -= damage;
    }

    private void OnTriggerEnter2D(Collider2D collision) 
    {
        // Damages player
        if (collision.gameObject.CompareTag("enemyProjectile") && !invulnerable)
        {
            Damage(35);
            Destroy(collision.gameObject);
            AudioPlayerScript.audioPlayerInstance.playPlayerHitSound();
			Instantiate(cameraShake, transform);
            toggleInvulnerable();
        }
    }

    private void Attack()
    {
        Vector3 aimPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        aimPos.z = 0;
        GameObject attack = Instantiate(projectile, shield.shotPoint.transform.position, shield.transform.rotation);
        attack.GetComponent<Rigidbody2D>().AddForce(shield.transform.right * shotSpeed);
        AudioPlayerScript.audioPlayerInstance.playPlayerShootingSound();
        GameObject shotFX = Instantiate(shotEffect, shield.shotPoint.transform);
        shotFX.transform.position = shield.shotPoint.transform.position;
    }


    public bool isMoving()
    {
        return moving;
    }

    public bool isGrounded()
    {
        return grounded;
    }

    public bool jumpSound()
    {
        return jumped & grounded;
    }

	public void SetDashAvailable(bool b) {
		dashAvailable = b;
	}

    private void toggleInvulnerable()
    {
        if (!invulnerable)
        {
            invulnerable = true;
            gameObject.GetComponent<SpriteRenderer>().color = new Color(255f, 255f, 255f, 0.5f);
            invTime = 0f;
        } else
        {
            invulnerable = false;
            gameObject.GetComponent<SpriteRenderer>().color = new Color(255f, 255f, 255f);
        }
    }

	public float getHealthAmount() {
		return health;
	}
}
