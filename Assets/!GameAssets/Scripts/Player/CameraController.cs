﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public Player player;
    private Vector3 offset;


	// Use this for initialization
	void Start () {
        offset = transform.position - player.transform.position;
		Player playerInstance = player.GetComponent<Player>();
    }


	// Update is called once per frame
	void LateUpdate () {
        transform.position = player.transform.position + offset;
		transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0f, 0f, 0f), Time.deltaTime*5); // Resets camera rotation to 0,0,0
    }


}
