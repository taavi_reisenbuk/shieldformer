﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttachedToPlayer : MonoBehaviour {
    // This script is really only necessary for the ShieldPivot object.

    public Transform following;
    
	void Start () {
        
	}
	
	void Update () {
        transform.position = following.transform.position;
	}
}
