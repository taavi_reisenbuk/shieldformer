﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class Editor : EditorWindow {

    int health = 0;
    float attackCooldown = 0;
    Color color = Color.red;

    [MenuItem ("Window/Editor")]

    public static void ShowWindow(){
        GetWindow(typeof(Editor));
    }

    private void OnGUI()
    {

        GUILayout.Label("Enemy settings", EditorStyles.boldLabel);
        GUILayout.Label("Health");
        health = EditorGUILayout.IntField(health);
        GUILayout.Label("Attack cooldown");
        attackCooldown = EditorGUILayout.Slider(attackCooldown, 0, 5);
        GUILayout.Label("Enemy color");
        color = EditorGUILayout.ColorField(color);

        if (GUILayout.Button("Edit"))
        {
            Edit();
        }
    }

    void Edit()
    {
        foreach (GameObject obj in Selection.gameObjects)
        {
            Enemy enemy = obj.GetComponent<Enemy>();
            SpriteRenderer renderer = obj.GetComponent<SpriteRenderer>();
            if(enemy != null)
            {
                enemy.health = health;
                enemy.attackCooldown = attackCooldown;
                if (renderer != null)
                    renderer.color = color;
            }
        }
    }
}
