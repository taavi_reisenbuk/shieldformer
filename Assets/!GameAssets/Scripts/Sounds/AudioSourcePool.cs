﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioSourcePool : MonoBehaviour {

    private List<AudioSource> audioSources; // can be private, doesn't have to be.

    private void Awake()
    {
        audioSources = new List<AudioSource>();
    }


    public AudioSource getSource()
    {
        for (int i = 0; i < audioSources.Count; i++)    // finds an audiosource that isn't playing at the time.
        {
            if (!audioSources[i].isPlaying)
            {
                return audioSources[i];
            }
        }

        // in case there's no audiosource that isn't playing at the time, we will create one.
        GameObject audioObject = new GameObject("AudioSource"); // creating an audiosource with a name. 
        AudioSource source = audioObject.AddComponent<AudioSource>(); // now we need to attach the audiosource component to the object.
        audioSources.Add(source); // this allows us to reuse our sounds.
        return source;
    }
}
