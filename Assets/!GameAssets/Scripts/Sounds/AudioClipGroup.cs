﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Game/AudioClipGroup")]
public class AudioClipGroup : ScriptableObject {

    [Range(0,2)]
    public float volumeMin = 1;

    [Range(0, 2)]
    public float volumeMax = 1;

    [Range(0, 2)]
    public float pitchMin = 1;

    [Range(0, 2)]
    public float pitchMax = 1;

    public float cooldown = 0.2f;
    public AudioClip[] audioClips;

    private float timeStamp;
    private AudioSourcePool audioSourcePool;


    void OnEnable() {
        audioSourcePool = FindObjectOfType<AudioSourcePool>();
        timeStamp = -cooldown;
	}

    public void Play()
    {
        if (audioSourcePool == null) // just a failsafe in case it's not found on the first time.
        {
            audioSourcePool = FindObjectOfType<AudioSourcePool>();
        }

        AudioSource source = audioSourcePool.getSource();
        source.transform.position = Vector3.zero;
        source.spatialBlend = 0;
        Play(source);
    }

    public void Play(Vector3 location) // Plays at position
    {
        if (audioSourcePool == null)
        {
            audioSourcePool = FindObjectOfType<AudioSourcePool>();
        }

        AudioSource source = audioSourcePool.getSource();
        source.transform.position = location;
        source.spatialBlend = 1;
        Play(source);
    }


    public void Play(AudioSource source)
    {
        if (audioClips.Length == 0) return;

        source.clip = audioClips[Random.Range(0, audioClips.Length)];

        // TODO: add cooldown
        if (Time.time < timeStamp + cooldown) return;
        timeStamp = Time.time;

        source.volume = Random.Range(volumeMin, volumeMax);
        source.pitch = Random.Range(pitchMin, pitchMax);
        source.Play();
    }
}
