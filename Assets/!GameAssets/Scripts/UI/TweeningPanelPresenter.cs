﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TweeningPanelPresenter : MonoBehaviour
{

    public ScalingAnimation OpeningAnimation;
    public ScalingAnimation scalingAnimation;

    public void Open()
    {
        if(gameObject.activeSelf)
            return;

        gameObject.SetActive(true);
        OpeningAnimation.enabled = true;
    }

    public void Close()
    {
        //TODO Return to the guide. Finish this method after the ScalingAnimation script
        if(!gameObject.activeSelf)
            return;

        scalingAnimation.enabled = true;

        //gameObject.SetActive(false);
    }

}
