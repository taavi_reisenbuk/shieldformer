﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UI : MonoBehaviour {

	public Player playerInstance;
    public static UI instance;
    public GameObject hpAndEnergyPanel;
    public GameObject inGameMenuPanel;

    public Image energyBar;
    public Image healthBar;
    public Text loseText;
    public Text winText;

	private void Awake()
    {
        setMenuActive(false);

        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

	// Update is called once per frame
	void Update () {
		playerInstance = FindObjectOfType<Player>();

		healthBar.fillAmount = playerInstance.getHealthAmount() / 100f;
		energyBar.fillAmount = playerInstance.shield.getEnergyAmount() / 100f;

		if (playerInstance.getHealthAmount() <= 0)
			{
				GameLost();
			}


		if (inGameMenuPanel.activeSelf == true & Input.GetKeyUp(KeyCode.Escape))
		{
		  setMenuActive(false);
		}


		else if (inGameMenuPanel.activeSelf == false & Input.GetKeyUp(KeyCode.Escape))
        {
            setMenuActive(true);
        }
	}

    public void setMenuActive(bool value)
    {
        inGameMenuPanel.SetActive(value);
    }

    public void exitToMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

	public void GameLost()
	{
		playerInstance.SetControlEnabled(false);
		//loseText.gameObject.SetActive(true);
	}

	public void GameWon()
	{
		playerInstance.SetControlEnabled(true);
		winText.gameObject.SetActive(true);
	}
}
