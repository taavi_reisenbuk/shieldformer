﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour {

    public MainMenuController instance;

	// Use this for initialization
	void Start () {
        instance = this;
	}
	

    public void continueGame()
    {
        SceneManager.LoadScene("Tutorial");
        UI.instance.setMenuActive(false);
    }

    public void newGame()
    {
        SceneManager.LoadScene("Tutorial");
        UI.instance.setMenuActive(false);
    }

    public void exitGameFromButton()
    {
        Application.Quit();
    }
}
