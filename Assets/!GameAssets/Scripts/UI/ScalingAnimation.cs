﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ScalingAnimation : MonoBehaviour {

    public AnimationCurve animationCurve;
    public float Speed;
    public Vector3 startScale;
    public Vector3 endScale;
    private float timeAggregate;
    public UnityEvent OnEndAction; 
    

	// Use this for initialization
	void OnEnable () {
        timeAggregate = 0f;
        transform.localScale = startScale;
	}
	
	// Update is called once per frame
	void Update () {
        timeAggregate += Time.deltaTime * Speed;
        float sample = animationCurve.Evaluate(timeAggregate);
        transform.localScale = Vector3.LerpUnclamped(startScale, endScale, sample);
        if (timeAggregate >= 1f) {
            enabled = false;
            OnEndAction.Invoke();
        }
	}
}
