﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHitDetection : MonoBehaviour {

    public Enemy parentTurret;
	public GameObject enemyHitEffect;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}



    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("friendlyProjectile"))
        {
            parentTurret.health -= 20;
			Instantiate(enemyHitEffect, parentTurret.GetComponentInParent<Transform>());
        }
    }
}
