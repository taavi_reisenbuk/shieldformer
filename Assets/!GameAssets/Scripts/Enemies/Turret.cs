﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : MonoBehaviour {

    public GameObject target;
    public double detectTime;
    private double warmupTime;
    private double time;
    private LineRenderer lr;
    public int maxMisses;
    private int missCounter;
    private string currentMode;
    public Vector3 angle;
    public float searchMultiplier;
    public int searchTime;

	// Use this for initialization
	void Start () {
        currentMode = "search";
        time = 0.0;
        lr = gameObject.GetComponent<LineRenderer>();
        lr.positionCount = 2;
        lr.SetPosition(0, transform.position);
        lr.SetPosition(1, new Vector3(transform.parent.position.x - 0.5f * searchTime * searchMultiplier, 0));
	}
	
	// Update is called once per frame
	void Update () {
        Debug.DrawRay(transform.parent.position, transform.TransformDirection(Vector3.forward), Color.red, 20, true);
        // Idle. Looking for a target.
		if (currentMode == "search")
        {
            if (time >= searchTime)
            {
                searchMultiplier = -searchMultiplier;
                time = 0.0;
            }
            angle.x += searchMultiplier * Time.deltaTime;
            lr.SetPosition(0, transform.position);
            lr.SetPosition(1, new Vector3((transform.parent.position.x - 0.5f * searchTime * Mathf.Abs(searchMultiplier)) + angle.x, angle.y));
            if (check())
            {
                warmupTime = 0.0;
                currentMode = "detect";
            }
        }
        // Spotted target, tracking.
        if (currentMode == "detect")
        {
            warmupTime += Time.deltaTime;
            angle = target.transform.position;
            lr.SetPosition(0, transform.position);
            lr.SetPosition(1, target.transform.position);
            if (check())
            {
                missCounter = 0;
                if (warmupTime >= detectTime)
                {
                    currentMode = "engaging";
                }
            } else
            {
                currentMode = "lost";
            }
        }
        // Lost detected target. Trying to reacquire.
        if (currentMode == "lost")
        {
            if (warmupTime > 0)
            {
                warmupTime -= Time.deltaTime;
            }
            lr.SetPosition(0, transform.position);
            lr.SetPosition(1, target.transform.position);
            if (check())
            {
                currentMode = "detect";
            }

        }
        // Target has been found.
        if (currentMode == "engaging")
        {

        }

        time += Time.deltaTime;
	}

    private bool check()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, new Vector3(transform.parent.position.x - 0.5f * searchTime * Mathf.Abs(searchMultiplier) + angle.x, angle.y), out hit))
        {
            print("collided with something");
            if (hit.transform.gameObject.CompareTag("Player"))
            {
                target = hit.transform.gameObject;
                return true;
            }
        }
        return false;
    }
}
