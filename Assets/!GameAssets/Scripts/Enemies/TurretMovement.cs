﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretMovement : MonoBehaviour {
    [Range(0f, 10f)] public float moveLeft;
    [Range(0f, 10f)] public float moveRight;
    [Range(0f, 5f)] public float speed;
    private Vector3 maxLeft;
    private Vector3 maxRight;
    private int direction = 1;

    // Use this for initialization
    void Start () {
		maxLeft = new Vector3(transform.position.x - moveLeft, transform.position.y);
        maxRight = new Vector3(transform.position.x + moveRight, transform.position.y);
    }
	
	// Update is called once per frame
	void Update () {
        if (transform.position == maxLeft || transform.position == maxRight)
        {
            direction *= -1;
        }
        if (direction == 1)
        {
            transform.position = Vector3.MoveTowards(transform.position, maxLeft, speed * Time.deltaTime);
        } else
        {
            transform.position = Vector3.MoveTowards(transform.position, maxRight, speed * Time.deltaTime);
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawLine(transform.position, new Vector3(transform.position.x - moveLeft, transform.position.y));
        Gizmos.DrawLine(transform.position, new Vector3(transform.position.x + moveRight, transform.position.y));
    }
}
