﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour {
    public int health = 100;
    private float maxHealth;
    public GameObject projectile;
    public Image healthBar;
    private float time = 0f;
    public float attackCooldown;
    private bool inRange = false;
    public int burst;
    private int burstCount;
    private float burstDelay = 0.1f;
    SpriteRenderer spriteRenderer;
    Color defaultColor;


    // Use this for initialization
    void Start () {
        spriteRenderer = this.gameObject.GetComponent<SpriteRenderer>();
        defaultColor = spriteRenderer.color;
        maxHealth = health;
        burstCount = burst;
    }
	
	// Update is called once per frame
	void Update () {

		if (health <= 0)
        {
			Destroy(this.gameObject);
        }

        spriteRenderer.color = new Color(defaultColor.r, (defaultColor.g - (maxHealth - health)*0.01f), (defaultColor.b - (maxHealth - health)*0.01f));

        if (inRange)
        {
            Vector3 vectorToTarget = Player.playerInstance.transform.position - transform.position;
            float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
            Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
            transform.rotation = Quaternion.Slerp(transform.rotation, q, Time.deltaTime * 5);
            Shoot();
        }

        healthBar.fillAmount = (health / maxHealth);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            inRange = true;
			AudioPlayerScript.audioPlayerInstance.playEnemyAlertedSound();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            inRange = false;
        }
    }


    private void Shoot()
    {
        time += Time.deltaTime;
        if (time >= attackCooldown)
        {
            GameObject attack = Instantiate(projectile, this.transform);
            attack.GetComponent<Rigidbody2D>().AddForce(new Vector2(transform.right.x, transform.right.y).normalized * 1f);

			if (this.gameObject.CompareTag("enemySimple")) {
				AudioPlayerScript.audioPlayerInstance.playEnemyShootingSound();
			}
			else if (this.gameObject.CompareTag("enemyBurst")) {
				AudioPlayerScript.audioPlayerInstance.playBurstEnemyShootingSound();
			}
			else if (this.gameObject.CompareTag("enemyRapid")) {
				AudioPlayerScript.audioPlayerInstance.playRapidEnemyShootingSound();
			}

			burstCount--;
            time = attackCooldown - burstDelay;
            if (burstCount < 1)
            {
                burstCount = burst;
                time = 0;
            }
        }
    }
}
