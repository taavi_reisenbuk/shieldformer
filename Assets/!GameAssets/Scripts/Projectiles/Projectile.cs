﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

	public ParticleSystem shotDestroyEffect;

	// Use this for initialization
	void Start () {
		
	}

	// Update is called once per frame
	void Update() {

	}

	private void OnBecameInvisible() {
		Destroy(gameObject);
	}


	private void OnTriggerEnter2D(Collider2D collision) {

		if (collision.gameObject.CompareTag("ground")) {
			AudioPlayerScript.audioPlayerInstance.playEnemyShotAgainstGround();
			Destroy(gameObject);
		}
	}
}
